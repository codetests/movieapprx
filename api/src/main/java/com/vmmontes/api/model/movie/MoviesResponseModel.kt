package com.vmmontes.api.model.movie

data class MoviesResponseModel(
    var moviesApiResponseModel: MoviesApiResponseModel?,
    var responseState: ResponseState
) {
    enum class ResponseState{
        SUCCESS, GENERIC_ERROR, MAX_PAGES, SERVER_ERROR
    }
}