package com.vmmontes.api

import com.vmmontes.api.model.movie.MoviesApiResponseModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface BaseAppApiService {

    @GET("search/movie")
    fun getFindMoviesList(@Query("api_key") apiKey: String,
                          @Query("language") language: String,
                          @Query("query") movie: String,
                          @Query("page") page: String): Call<MoviesApiResponseModel>
}