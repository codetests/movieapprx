package com.vmmontes.moviesapp.presentation.movieslist

import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel
import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import com.vmmontes.moviesapp.domain.usecase.*
import com.vmmontes.moviesapp.kernel.coroutines.UncaughtCoRoutineExceptionHandler
import com.vmmontes.moviesapp.kernel.coroutines.backgroundContext
import com.vmmontes.moviesapp.kernel.coroutines.mainContext
import com.vmmontes.moviesapp.presentation.presenter.movieslist.MoviesListPresenter
import com.vmmontes.moviesapp.presentation.ui.movieslist.MoviesListView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Unconfined
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class MoviesListUnitTest {

    private lateinit var view: MoviesListView
    private lateinit var getMoviesListUseCase: GetMoviesListUseCase
    private lateinit var addMoviesToStoredListUseCase: AddMoviesToStoredListUseCase
    private lateinit var clearStoredMoviesUseCase: ClearStoredMoviesUseCase
    private lateinit var getStoredMoviesUseCase: GetStoredMoviesUseCase
    private lateinit var isSameLastQuery: IsSameLastQuery
    private lateinit var presenter: MoviesListPresenter

    @Before
    fun setup() {
        getMoviesListUseCase = Mockito.mock(GetMoviesListUseCase::class.java)
        addMoviesToStoredListUseCase = Mockito.mock(AddMoviesToStoredListUseCase::class.java)
        clearStoredMoviesUseCase = Mockito.mock(ClearStoredMoviesUseCase::class.java)
        getStoredMoviesUseCase = Mockito.mock(GetStoredMoviesUseCase::class.java)
        isSameLastQuery = Mockito.mock(IsSameLastQuery::class.java)
        presenter = MoviesListPresenter(getMoviesListUseCase,
            addMoviesToStoredListUseCase, clearStoredMoviesUseCase,
            isSameLastQuery)
        view = Mockito.mock(MoviesListView::class.java)
        backgroundContext = Dispatchers.IO + UncaughtCoRoutineExceptionHandler()
        mainContext = Dispatchers.Main + UncaughtCoRoutineExceptionHandler()
    }

    @Test
    fun `should show loading and hide rest of views, finally should call list movies withour clear stored movies`(){
        // arrange
        backgroundContext = Unconfined
        presenter.onAttach(view)

        // act
        presenter.onQueryTextSubmit("Es", "prueba", "prueba2")

        // assert
        Mockito.verify(view, Mockito.times(1)).hideKeyboard()
        Mockito.verify(view, Mockito.times(1)).showLoading()
        Mockito.verify(view, Mockito.times(1)).hideMessageText()
        Mockito.verify(view, Mockito.times(1)).hideFindFilmInformation()
        Mockito.verify(isSameLastQuery, Mockito.times(1)).execute("prueba", "prueba2")
        Mockito.verify(clearStoredMoviesUseCase, Mockito.never()).execute()
        Mockito.verify(getMoviesListUseCase, Mockito.times(1)).execute("Es", "prueba")
    }

    @Test
    fun `should show loading and hide rest of views, finally should call list movies with clear stored movies`(){
        // arrange
        backgroundContext = Unconfined
        presenter.onAttach(view)

        // act
        presenter.onQueryTextSubmit("Es", "prueba", "prueba")

        // assert
        Mockito.verify(view, Mockito.times(1)).hideKeyboard()
        Mockito.verify(view, Mockito.times(1)).showLoading()
        Mockito.verify(view, Mockito.times(1)).hideMessageText()
        Mockito.verify(view, Mockito.times(1)).hideFindFilmInformation()
        Mockito.verify(isSameLastQuery, Mockito.times(1)).execute("prueba", "prueba")
        Mockito.verify(clearStoredMoviesUseCase, Mockito.times(1)).execute()
        Mockito.verify(getMoviesListUseCase, Mockito.times(1)).execute("Es", "prueba")
    }

    @Test
    fun `should call list movies`(){
        // arrange
        backgroundContext = Unconfined
        presenter.onAttach(view)

        // act
        presenter.showMoreItems("Es", "prueba")

        // assert
        Mockito.verify(getMoviesListUseCase, Mockito.times(1)).execute("Es", "prueba")
    }

    @Test
    fun `should add movies to store, hide loading and show movies in list`(){
        // arrange
        val moviesList: MutableList<MovieDomainModel> = mutableListOf()
        mainContext = Unconfined
        presenter.onAttach(view)

        // act
        presenter.isSuccess(moviesList)

        // assert
        Mockito.verify(addMoviesToStoredListUseCase, Mockito.times(1)).execute(moviesList)
        Mockito.verify(view, Mockito.times(1)).hideLoading()
        Mockito.verify(view, Mockito.times(1)).showMoviesIntoList(moviesList)
    }

    @Test
    fun `should show empty message, and clean view list`(){
        // arrange
        mainContext = Unconfined
        presenter.onAttach(view)

        // act
        presenter.isEmpty()

        // assert
        Mockito.verify(view, Mockito.times(1)).showIsEmpty()
        Mockito.verify(view, Mockito.times(1)).hideLoading()
        Mockito.verify(view, Mockito.times(1)).cleanList()
    }

    @Test
    fun `should show error message`(){
        // arrange
        val errorExample: ListMovieDomainModel.MoviesListState = ListMovieDomainModel.MoviesListState.GENERIC_ERROR
        mainContext = Unconfined
        presenter.onAttach(view)

        // act
        presenter.error()

        // assert
        Mockito.verify(view, Mockito.times(1)).showError()
        Mockito.verify(view, Mockito.times(1)).hideLoading()
        Mockito.verify(view, Mockito.times(1)).cleanList()
    }

    @Test
    fun `should open detail view`(){
        // arrange
        val movieExample: MovieDomainModel =
            MovieDomainModel(1, "url", false, "date",
                "title", "overview", 5.0, 2, "original")
        presenter.onAttach(view)

        // act
        presenter.onMovieListAction(movieExample)

        // assert
        Mockito.verify(view, Mockito.times(1)).openDetailMoview(movieExample.id)
    }
}
