package com.vmmontes.moviesapp

import android.app.Application
import com.vmmontes.moviesapp.di.*

class MoviesApplication : Application() {

    val component : ApplicationComponent by lazy {
        DaggerApplicationComponent
            .builder()
            .applicationModule(ApplicationModule(this))
            .moviesListModule(MoviesListModule())
            .detailMovieModule(DetailMovieModule())
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}