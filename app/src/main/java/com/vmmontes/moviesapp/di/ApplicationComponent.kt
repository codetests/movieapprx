package com.vmmontes.moviesapp.di

import com.vmmontes.moviesapp.MoviesApplication
import com.vmmontes.moviesapp.presentation.ui.moviedetail.MovieDetailFragment
import com.vmmontes.moviesapp.presentation.ui.moviedetail.MovieDetailView
import com.vmmontes.moviesapp.presentation.ui.movieslist.MoviesListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, MoviesListModule::class, DetailMovieModule::class])
interface ApplicationComponent {
    fun inject(app: MoviesApplication)
    fun inject(moviesListView: MoviesListFragment)
    fun inject(movieDetailView: MovieDetailFragment)
}