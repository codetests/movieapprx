package com.vmmontes.moviesapp.di

import com.vmmontes.api.RetrofitService
import com.vmmontes.api.client.MoviesClient
import com.vmmontes.api.client.MoviesClientImp
import com.vmmontes.moviesapp.data.datasource.cloud.MoviesCloudDataSource
import com.vmmontes.moviesapp.data.datasource.cloud.MoviesCloudDataSourceImp
import com.vmmontes.moviesapp.data.datasource.local.MoviesLocalDataSource
import com.vmmontes.moviesapp.data.datasource.local.MoviesLocalDataSourceImp
import com.vmmontes.moviesapp.data.repository.MoviesRepository
import com.vmmontes.moviesapp.data.repository.MoviesRepositoryImp
import com.vmmontes.moviesapp.domain.usecase.*
import com.vmmontes.moviesapp.presentation.presenter.movieslist.MoviesListPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MoviesListModule {

    @Provides
    fun provideMoviesClient(apiService: RetrofitService): MoviesClient = MoviesClientImp(apiService)

    @Provides
    @Singleton
    fun provideMoviesLocalDataSource(): MoviesLocalDataSource = MoviesLocalDataSourceImp()

    @Provides
    fun providesMoviesCloudDataSource(moviesClient: MoviesClient): MoviesCloudDataSource =
        MoviesCloudDataSourceImp(moviesClient)

    @Provides
    fun providesMoviesRepository(moviesCloudDataSource: MoviesCloudDataSource,
                                 moviesLocalDataSource: MoviesLocalDataSource): MoviesRepository =
        MoviesRepositoryImp(moviesCloudDataSource, moviesLocalDataSource)

    @Provides
    fun providesAddMoviesToStoredListUseCase(moviesRepository: MoviesRepository): AddMoviesToStoredListUseCase =
        AddMoviesToStoredListUseCase(moviesRepository)

    @Provides
    fun providesGetMoviesListUseCase(moviesRepository: MoviesRepository): GetMoviesListUseCase =
        GetMoviesListUseCase(moviesRepository)

    @Provides
    fun providesClearStoredMoviesUseCase(moviesRepository: MoviesRepository): ClearStoredMoviesUseCase =
        ClearStoredMoviesUseCase(moviesRepository)

    @Provides
    fun providesGetStoredMoviesUseCase(moviesRepository: MoviesRepository): GetStoredMoviesUseCase =
        GetStoredMoviesUseCase(moviesRepository)

    @Provides
    fun providesIsSameLastQuery(): IsSameLastQuery =
        IsSameLastQuery()

    @Provides
    fun provideMoviesListPresenter(getMoviesListUseCase: GetMoviesListUseCase,
                                   addMoviesToStoredListUseCase: AddMoviesToStoredListUseCase,
                                   clearStoredMoviesUseCase: ClearStoredMoviesUseCase,
                                   isSameLastQuery: IsSameLastQuery
    ): MoviesListPresenter =
        MoviesListPresenter(getMoviesListUseCase, addMoviesToStoredListUseCase, clearStoredMoviesUseCase, isSameLastQuery)
}