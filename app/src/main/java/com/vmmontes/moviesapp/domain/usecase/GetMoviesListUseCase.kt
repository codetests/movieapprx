package com.vmmontes.moviesapp.domain.usecase

import com.vmmontes.moviesapp.data.repository.MoviesRepository
import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel
import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import com.vmmontes.moviesapp.presentation.presenter.model.MovieListViewModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers

class GetMoviesListUseCase(val moviesRepository: MoviesRepository) {

    fun execute(language: String, movieName: String): Observable<MovieListViewModel> {
        return moviesRepository.getMovies(language, movieName)
            .map {
                val stateViewModel = when(it.moviesListState){
                    ListMovieDomainModel.MoviesListState.SUCCESS -> {
                        if (it.moviesList.size > 0) {
                            MovieListViewModel.MoviesListStateViewModel.LIST_WITH_DATA
                        } else {
                            MovieListViewModel.MoviesListStateViewModel.EMPTY_LIST
                        }
                    }
                    ListMovieDomainModel.MoviesListState.GENERIC_ERROR -> MovieListViewModel.MoviesListStateViewModel.GENERIC_ERROR
                    ListMovieDomainModel.MoviesListState.SERVER_ERROR -> MovieListViewModel.MoviesListStateViewModel.SERVER_ERROR
                    ListMovieDomainModel.MoviesListState.MAX_PAGES -> MovieListViewModel.MoviesListStateViewModel.MAX_PAGES
                }

                MovieListViewModel(it.moviesList, stateViewModel)
            }.observeOn(AndroidSchedulers.mainThread())
    }

    interface MoviesResponse {
        fun isSuccess(moviesList: MutableList<MovieDomainModel>)
        fun isEmpty()
        fun isLimitOfPagination()
        fun error()
    }
}