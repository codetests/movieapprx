package com.vmmontes.moviesapp.domain.usecase

import com.vmmontes.moviesapp.data.repository.MoviesRepository
import com.vmmontes.moviesapp.data.repository.MoviesRepositoryImp
import com.vmmontes.moviesapp.domain.model.MovieDomainModel

class ClearStoredMoviesUseCase(val moviesRepository: MoviesRepository ) {

    fun execute() {
        moviesRepository.cleanStoredList()
    }
}