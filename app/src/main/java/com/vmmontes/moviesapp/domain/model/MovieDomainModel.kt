package com.vmmontes.moviesapp.domain.model

data class MovieDomainModel(
    val id: Int,
    val posterPath: String?,
    val adult: Boolean,
    val releaseDate: String,
    val title: String,
    val overview: String,
    val voteAverage: Double,
    val voteCount: Int,
    val originalTitle: String
)