package com.vmmontes.moviesapp.domain.model

data class ListMovieDomainModel(
    val moviesList: MutableList<MovieDomainModel>,
    val moviesListState: MoviesListState
){
    enum class MoviesListState{
        SUCCESS, GENERIC_ERROR, MAX_PAGES, SERVER_ERROR
    }
}