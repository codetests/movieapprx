package com.vmmontes.moviesapp.data.mapper

import com.vmmontes.api.model.movie.MovieApiModel
import com.vmmontes.api.model.movie.MoviesApiResponseModel
import com.vmmontes.api.model.movie.MoviesResponseModel
import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel
import com.vmmontes.moviesapp.domain.model.MovieDomainModel

object MovieMapper{

    fun movieToDomain(movieApiModel: MovieApiModel): MovieDomainModel = MovieDomainModel(
        movieApiModel.id,
        movieApiModel.posterPath,
        movieApiModel.adult,
        movieApiModel.releaseDate,
        movieApiModel.title,
        movieApiModel.overview,
        movieApiModel.voteAverage,
        movieApiModel.voteCount,
        movieApiModel.originalTitle
    )

    fun moviesListToDomain(moviewsResponseModel: MoviesResponseModel): ListMovieDomainModel{
        val movieApiResponseModel = moviewsResponseModel.moviesApiResponseModel
        val list = ListMovieDomainModel(mutableListOf(), apiStateToDomain(moviewsResponseModel.responseState))

        movieApiResponseModel?.results.also {
            it?.also {moviesApiModel -> ArrayList<MovieApiModel>()
                for(movieApiModel in moviesApiModel) {
                    val movieDomainModel = movieToDomain(movieApiModel)
                    list.moviesList.add(movieDomainModel)
                }
            }
        }
        return list
    }

    fun apiStateToDomain(responseState: MoviesResponseModel.ResponseState): ListMovieDomainModel.MoviesListState =
        when (responseState) {
            MoviesResponseModel.ResponseState.SUCCESS -> ListMovieDomainModel.MoviesListState.SUCCESS
            MoviesResponseModel.ResponseState.GENERIC_ERROR -> ListMovieDomainModel.MoviesListState.GENERIC_ERROR
            MoviesResponseModel.ResponseState.SERVER_ERROR -> ListMovieDomainModel.MoviesListState.GENERIC_ERROR
            MoviesResponseModel.ResponseState.MAX_PAGES -> ListMovieDomainModel.MoviesListState.MAX_PAGES
        }
}