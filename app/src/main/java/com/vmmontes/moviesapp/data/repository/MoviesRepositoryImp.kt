package com.vmmontes.moviesapp.data.repository

import com.vmmontes.moviesapp.data.datasource.cloud.MoviesCloudDataSource
import com.vmmontes.moviesapp.data.datasource.local.MoviesLocalDataSource
import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel
import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class MoviesRepositoryImp(val moviesCloudDataSource: MoviesCloudDataSource,
                          val moviesLocalDataSource: MoviesLocalDataSource): MoviesRepository {


    override fun getMovies(language: String, movieName: String): Observable<ListMovieDomainModel> {
        return Observable.create<ListMovieDomainModel> {
            val listMovieDomainModel = moviesCloudDataSource.getMovies(language, movieName)
            it.onNext(listMovieDomainModel)
        }.subscribeOn(Schedulers.io())
    }

    override fun getMovie(id: Int): MovieDomainModel? = moviesLocalDataSource.getMovie(id)

    override fun getStoredList(): MutableList<MovieDomainModel> = moviesLocalDataSource.getList()

    override fun addMoviesIntoStoredList(moviesList: MutableList<MovieDomainModel>) {
        moviesLocalDataSource.addall(moviesList)
    }

    override fun cleanStoredList() {
        moviesLocalDataSource.clearList()
    }
}