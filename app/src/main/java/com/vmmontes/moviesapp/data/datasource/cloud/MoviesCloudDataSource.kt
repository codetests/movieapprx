package com.vmmontes.moviesapp.data.datasource.cloud

import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel

interface MoviesCloudDataSource {
    fun getMovies(language: String, movieName: String): ListMovieDomainModel
}