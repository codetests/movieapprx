package com.vmmontes.moviesapp.data.datasource.local

import com.vmmontes.moviesapp.domain.model.MovieDomainModel

class MoviesLocalDataSourceImp: MoviesLocalDataSource {
    companion object {
        private var moviesList = mutableListOf<MovieDomainModel>()
    }

    override fun getList(): MutableList<MovieDomainModel> = moviesList

    override fun getMovie(id: Int): MovieDomainModel? = moviesList.find { it.id == id }

    override fun addall(list: MutableList<MovieDomainModel>) {
        moviesList.addAll(list)
    }

    override fun clearList() {
        moviesList.clear()
    }
}