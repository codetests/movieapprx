package com.vmmontes.moviesapp.data.datasource.local

import com.vmmontes.moviesapp.domain.model.MovieDomainModel

interface MoviesLocalDataSource {
    fun getList(): MutableList<MovieDomainModel>
    fun addall(list: MutableList<MovieDomainModel>)
    fun getMovie(id: Int): MovieDomainModel?
    fun clearList()
}