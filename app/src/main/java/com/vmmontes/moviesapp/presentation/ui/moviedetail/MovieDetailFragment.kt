package com.vmmontes.moviesapp.presentation.ui.moviedetail

import android.os.Bundle
import android.view.*
import com.vmmontes.api.kernel.BASE_IMG_URL
import com.vmmontes.moviesapp.R
import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import com.vmmontes.moviesapp.kernel.ui.BaseFragment
import com.vmmontes.moviesapp.presentation.presenter.moviedetail.MovieDetailPresenter
import kotlinx.android.synthetic.main.fragment_detail_movie.*
import javax.inject.Inject

class MovieDetailFragment : BaseFragment(), MovieDetailView {
    @Inject
    lateinit var presenter: MovieDetailPresenter

    companion object {
        const val KEY_INT_DETAIL = "KEY_INT_DETAIL"

        @JvmStatic
        fun newInstance(id: Int) = MovieDetailFragment().apply {
            arguments = Bundle().apply {
                putInt(KEY_INT_DETAIL, id)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail_movie, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getApplication().component.inject(this)
        presenter.onAttach(this)
        arguments?.getInt(KEY_INT_DETAIL)?.also {id -> Int
            presenter.onViewCreated(id)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu!!.clear()
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun showMovie(movie: MovieDomainModel) {
        val urlImg = "$BASE_IMG_URL${movie.posterPath}"
        (activity!! as MovieDetailActivity).setImg(urlImg)
        textViewTitle.text = movie.title
        textViewOverview.text = movie.overview
        textViewRating.text = movie.voteAverage.toString()
        textViewVoteCount.text = movie.voteCount.toString()
        textViewOriginalTitle.text = movie.originalTitle
    }
}
