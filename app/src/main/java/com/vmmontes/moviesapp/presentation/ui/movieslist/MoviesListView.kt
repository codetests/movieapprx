package com.vmmontes.moviesapp.presentation.ui.movieslist

import com.vmmontes.moviesapp.domain.model.MovieDomainModel

interface MoviesListView {
    fun showError()
    fun hideMessageText()
    fun showLoading()
    fun hideLoading()
    fun hideKeyboard()
    fun showIsEmpty()
    fun showMoviesIntoList(moviesList: MutableList<MovieDomainModel>)
    fun cleanList()
    fun openDetailMoview(id: Int)
    fun hideFindFilmInformation()
}