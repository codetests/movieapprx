package com.vmmontes.moviesapp.presentation.ui.movieslist

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.vmmontes.moviesapp.R
import kotlinx.android.synthetic.main.activity_with_toolbar.*

class MoviesListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_with_toolbar)
        setSupportActionBar(container_toolbar)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.view_frame, MoviesListFragment.newInstance(), MoviesListFragment::class.java.name)
                .commit()
        }
    }
}
