package com.vmmontes.moviesapp.presentation.presenter.movieslist

import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import com.vmmontes.moviesapp.domain.usecase.*
import com.vmmontes.moviesapp.kernel.presenter.RxPresenter
import com.vmmontes.moviesapp.presentation.presenter.model.MovieListViewModel
import com.vmmontes.moviesapp.presentation.ui.movieslist.MoviesListView

class MoviesListPresenter(
    val getMoviesListUseCase: GetMoviesListUseCase,
    val addMoviesToStoredListUseCase: AddMoviesToStoredListUseCase,
    val clearStoredMoviesUseCase: ClearStoredMoviesUseCase,
    val isSameLastQuery: IsSameLastQuery
): RxPresenter<MoviesListView>(), GetMoviesListUseCase.MoviesResponse{

    private fun resetInitialValues() {
        clearStoredMoviesUseCase.execute()
        view?.cleanList()
    }

    private fun getMovies(language: String, query: String) {
        addDisposable(getMoviesListUseCase.execute(language, query)
            .subscribe{movieListViewModel ->
                val moviesList = movieListViewModel.moviesList
                when(movieListViewModel.moviesListState) {
                    MovieListViewModel.MoviesListStateViewModel.LIST_WITH_DATA -> isSuccess(moviesList)
                    MovieListViewModel.MoviesListStateViewModel.EMPTY_LIST -> isEmpty()
                    MovieListViewModel.MoviesListStateViewModel.GENERIC_ERROR -> error()
                    MovieListViewModel.MoviesListStateViewModel.SERVER_ERROR -> error()
                    MovieListViewModel.MoviesListStateViewModel.MAX_PAGES -> isLimitOfPagination()
                }
            })
    }

    private fun showMoviesIntoList(moviesList: MutableList<MovieDomainModel>) {
        view?.hideLoading()
        view?.showMoviesIntoList(moviesList)
    }

    fun onQueryTextSubmit(language: String, query: String, lastQuery: String) {
        if (!isSameLastQuery.execute(query, lastQuery)) {
            resetInitialValues()
        }
        view?.hideKeyboard()
        view?.showLoading()
        view?.hideMessageText()
        view?.hideFindFilmInformation()
        getMovies(language, query)
    }

    fun showMoreItems(language: String, movie: String) {
        getMovies(language, movie)
    }

    override fun isSuccess(moviesList: MutableList<MovieDomainModel>) {
        addMoviesToStoredListUseCase.execute(moviesList)
        showMoviesIntoList(moviesList)
    }

    override fun isEmpty() {
        view?.cleanList()
        view?.hideLoading()
        view?.showIsEmpty()
    }

    override fun error() {
        view?.cleanList()
        view?.hideLoading()
        view?.showError()
    }

    override fun isLimitOfPagination() {
        view?.hideLoading()
    }

    fun onMovieListAction(movie: MovieDomainModel) {
        view?.openDetailMoview(movie.id)
    }
}