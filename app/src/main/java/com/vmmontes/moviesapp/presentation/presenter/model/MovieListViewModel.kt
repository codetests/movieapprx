package com.vmmontes.moviesapp.presentation.presenter.model

import com.vmmontes.moviesapp.domain.model.MovieDomainModel

data class MovieListViewModel(
    val moviesList: MutableList<MovieDomainModel>,
    val moviesListState: MoviesListStateViewModel
){
    enum class MoviesListStateViewModel{
        LIST_WITH_DATA, GENERIC_ERROR, EMPTY_LIST, MAX_PAGES, SERVER_ERROR
    }
}